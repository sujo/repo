#!/bin/sh -f
#
# POSIX sh X setter
# Written by Michael Czigler <https://mcpcpc.github.io>

img() {
	[ -d "$1" ] && {
		set +f
		set -f -- "$1/"*
		shift "$(shuf -i "1-$#" -n 1)"
	}

	[ -f "${img:=$1}" ] || exit 1

	printf '%s\n' "$img"
}

main() {
	img "$1"
	display \
		-page 3200x \
		-sample 3200x \
		-window root \
		"$img" &
}

main "$1"
